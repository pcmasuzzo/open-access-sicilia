# open-access-sicilia
A repo with some code to get info on OA landscape in Sicily.
Go to this [notebook](open_access_sicilia.ipynb).

## launch this repo on Binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/pcmasuzzo%2Fopen-access-sicilia/HEAD)